const express = require('express');
const routes = require('./appRouter');
const fs = require('fs');
const Logger = require('./logger');

const port = 4000;
const app = express();
app.use(express.json());

const logger = new Logger();
logger.logAPICalls(app);
logger.on('log', ({fileName, content}) => {
    fs.appendFile(fileName, content, (err) => {
        if (err) console.log(err);
    });
});


app.use('/', routes);
app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});
