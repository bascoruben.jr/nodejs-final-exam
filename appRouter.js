const express = require('express');
const eventRouter = require('./routers/eventRouter');
const memberRouter = require('./routers/memberRouter');
const attendanceRouter = require('./routers/attendanceRouter');

const router = express.Router();

router.use('/events', eventRouter);
router.use('/members', memberRouter);
router.use('/attendance', attendanceRouter);

module.exports = router;