const DataStore = require('../datastore/dataStore');
const moment = require('moment');

const attendanceDataStore = new DataStore('attendance', []);

exports.insertAttendance = (req, res) => {
    const attendance = req.body;
    const attendanceId = attendanceDataStore.generateId();

    attendanceDataStore.insert({
        attendanceId,
        ...attendance
    });

    res.status(201).send("Successfully added!");
};

exports.updateAttendance = (req, res) => {
    const attendanceId = req.body.attendanceId;
    const attendance = { attendanceId, ...req.body };

    attendanceDataStore.update({ attendanceId }, attendance);

    res.status(200).send("Successfully updated!");
};

exports.deleteAttendance = (req, res) => {
    const attendanceId = req.body.attendanceId;

    attendanceDataStore.remove({ attendanceId });

    res.status(200).send("Successfully deleted!");
};

//========================Validator========================//

exports.attendanceIdValidator = (req, res, next) => {
    const attendanceId = req.params.attendanceId ? req.params.attendanceId : req.body.attendanceId;

    if (!attendanceId) {
        res.status(400).send('Please provide Attendance ID');
    } else {
        const isAttendanceExisting = attendanceDataStore.search({ attendanceId });
        if (!isAttendanceExisting.length) {
            res.status(404).send('Attendance not existing');
        } else {
            next();
        }
    }
};

exports.attendanceValidator = (req, res, next) => {
    const requiredProps = ['timeIn'];
    const bodyProps = Object.keys(req.body);

    const propsAreValid = requiredProps.every(propName => bodyProps.includes(propName) && req.body[propName]);

    if (!propsAreValid) {
        res.status(400).send('Missing properties');
    } else if (isDateValid(res, req.body) && isMemberExisting(res, req)) {
        next();
    }
};

//========================Helper========================//

const isDateValid = (res, body) => {
    let isValid = false;
    const hasTimeOut = body.timeOut;

    if (hasTimeOut) {
        try {
            const timeIn = moment(body.timeIn, ["h:mm A"]);
            const timeOut = moment(body.timeOut, ["h:mm A"]);
            timeIn < timeOut ? isValid = true : res.status(400).send(`Time Out should be greater than Time In`);
        } catch (e) {
            res.status(400).send(`Invalid Time`);
        }
    } else {
        isValid = true;
    }
    return isValid;
}

const isMemberExisting = (res, req) => {
    let isValid = false;
    if (req.route.methods.post) {
        const memberId = req.body.memberId;
        const eventId = req.body.eventId;

        const isMemberExisting = attendanceDataStore.search({ memberId, eventId });
        isMemberExisting.length ? res.status(400).send('Member already has attendance on the event') : isValid = true;
    } else {
        isValid = true;
    }

    return isValid;
}