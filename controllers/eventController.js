const DataStore = require('../datastore/dataStore');
const fs = require('fs');
const path = require('path');
const excel = require('excel4node');
const moment = require('moment');

const eventDataStore = new DataStore('events', []);

exports.getAllEvents = (req, res) => {
    const events = eventDataStore.getAll();

    res.status(200).send(events);
};

exports.getEventById = (req, res) => {
    const eventId = req.params.eventId;
    const response = getEventMemberAttendance(eventId);

    res.status(200).send(response);
};

exports.exportEventMemberAttendance = (req, res) => {
    const eventId = req.params.eventId;
    const response = getEventMemberAttendance(eventId);
    const memberAttendance = response['memberAttendance'] && response['memberAttendance'].length ? response['memberAttendance'].sort((a, b) => (moment(a.timeIn, ["h:mm A"]) > moment(b.timeIn, ["h:mm A"])) ? 1 : -1) : [];

    const name = `${response.eventName}_${moment(response.startDateTime).format('YYYY-MM-DD h-mm A')}`;
    const workbook = new excel.Workbook();
    const ws = workbook.addWorksheet(name);

    const style = workbook.createStyle({
        font: { color: '#000000', size: 10 }
    });

    ws.cell(1, 1).string('Member Name').style(style);
    ws.cell(1, 2).string('Time-In').style(style);
    ws.cell(1, 3).string('Time-Out').style(style);

    let row = 2;
    memberAttendance.forEach(record => {
        ws.cell(row, 1).string(record['name'] ? record['name'] : '').style(style);
        ws.cell(row, 2).string(record['timeIn'] ? record['timeIn'] : '').style(style);
        ws.cell(row, 3).string(record['timeOut'] ? record['timeOut'] : '').style(style);
        row++;
    })

    const outputsDir = path.join(__dirname, '../excel_exports');
    if (!fs.existsSync(outputsDir)) {
        fs.mkdirSync(outputsDir);
    }

    workbook.write(`excel_exports\\${name}.xlsx`, (err, stats) => {
        if (err) {
            res.status(400).send("Error in exporting!");
            console.error(err);
        } else {
            res.status(201).send("Successfully exported!");
        }
    });
}

exports.searchEvent = (req, res) => {
    const params = req.query;
    const searchParam = {};
    Object.keys(params).forEach(key => {
        switch (key) {
            case 'eventname': searchParam['eventName'] = params['eventname']; break;
            case 'datestart': searchParam['startDateTime'] = params['datestart']; break;
            case 'dateend': searchParam['endDateTime'] = params['dateend']; break;
            default: searchParam[key] = params[key];
        }
    });
    const events = eventDataStore.search(searchParam);
    res.status(200).send(events);
}

exports.insertEvent = (req, res) => {
    const event = req.body;
    const eventId = eventDataStore.generateId();

    eventDataStore.insert({
        eventId,
        eventName: event.eventName,
        eventType: event.eventType,
        startDateTime: new Date(event.startDateTime),
        endDateTime: new Date(event.endDateTime)
    });

    res.status(201).send("Successfully added!");
};

exports.updateEvent = (req, res) => {
    const eventId = req.body.eventId;
    const event = { eventId, ...req.body };

    eventDataStore.update({ eventId }, event);

    res.status(200).send("Successfully updated!");
};

exports.deleteEvent = (req, res) => {
    const eventId = req.body.eventId;

    eventDataStore.remove({ eventId });

    res.status(200).send("Successfully deleted!");
};

//========================Validator========================//

exports.searchEventValidator = (req, res, next) => {
    const params = req.query;

    if (Object.keys(params).length === 0) {
        res.status(400).send('Please provide search criteria!');
    } else {
        next();
    }
};

exports.eventIdValidator = (req, res, next) => {
    const eventId = req.params.eventId ? req.params.eventId : req.body.eventId;

    if (!eventId) {
        res.status(400).send('Please provide Event ID');
    } else {
        const isEventExisting = eventDataStore.search({ eventId });
        if (!isEventExisting.length) {
            res.status(404).send('Event not existing');
        } else {
            next();
        }
    }
};

exports.eventValidator = (req, res, next) => {
    const requiredProps = ['eventName', 'eventType', 'startDateTime', 'endDateTime'];
    const bodyProps = Object.keys(req.body);

    const propsAreValid = requiredProps.every(propName => bodyProps.includes(propName) && req.body[propName]);

    if (!propsAreValid) {
        res.status(400).send('Missing properties');
    } else if (isDateValid(res, req.body)) {
        next();
    }
};

//========================Helper========================//

const getEventMemberAttendance = (eventId) => {
    const event = eventDataStore.search({ eventId });
    const attendanceDataStore = new DataStore('attendance', []);
    const attendanceList = attendanceDataStore.search({ eventId });

    const memberDataStore = new DataStore('members', []);
    const memberList = memberDataStore.getAll().filter(member => attendanceList.some(attendance => attendance.memberId === member.memberId));

    const response = JSON.parse(JSON.stringify(event[0]));
    if (memberList.length) {
        response['memberAttendance'] = [];

        memberList.forEach(member => {
            const attendance = attendanceList.find(attendance => attendance.memberId === member.memberId);
            response['memberAttendance'].push({
                memberId: member.memberId,
                name: member.name,
                timeIn: attendance.timeIn,
                timeOut: attendance.timeOut
            });
        })
    }
    

    return response;
}

const isDateValid = (res, body) => {
    let isValid = false;
    const startDate = new Date(body.startDateTime);
    const startTime = startDate.getTime();
    const endDate = new Date(body.endDateTime);
    const endTime = endDate.getTime();

    if (startDate >= endDate && startTime >= endTime) {
        res.status(400).send('End Date should be greater than End Date');
    } else {
        isValid = true;
    }

    return isValid;
}

