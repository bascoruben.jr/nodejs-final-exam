const DataStore = require('../datastore/dataStore');

const memberDataStore = new DataStore('members', []);

exports.getAllMembers = (req, res) => {
    const members = memberDataStore.getAll();

    res.status(200).send(members);
};

exports.getMemberById = (req, res) => {
    const memberId = req.params.memberId;
    const member = memberDataStore.search({memberId});

    const attendanceDataStore = new DataStore('attendance', []);
    const attendanceList = attendanceDataStore.search({memberId});

    const eventDataStore = new DataStore('events', []);
    const eventList = eventDataStore.getAll().filter(event => attendanceList.some(attendance => attendance.eventId === event.eventId));
    
    const response = JSON.parse(JSON.stringify(member[0]));
    if (eventList.length) {
        response['eventAttendance'] = [];
        eventList.forEach(event => {
            const attendance = attendanceList.find(attendance => attendance.eventId === event.eventId);
            response['eventAttendance'].push({
                eventName: event.eventName,
                timeIn: attendance.timeIn,
                timeOut: attendance.timeOut
            });
        })

    }

    res.status(200).send(response);
};

exports.searchMember = (req, res) => {
    const params = req.query;
    const members = memberDataStore.search(params);
    res.status(200).send(members);
}

exports.insertMember = (req, res) => {
    const member = req.body;
    const memberId = memberDataStore.generateId();

    memberDataStore.insert({
        memberId,
        ...member
    });

    res.status(201).send("Successfully added!");
};

exports.updateMember = (req, res) => {
    const memberId = req.body.memberId;
    const member = { memberId, ...req.body };

    memberDataStore.update({memberId}, member);

    res.status(200).send("Successfully updated!"); 
};

exports.deleteMember = (req, res) => {
    const memberId = req.body.memberId;

    memberDataStore.remove({memberId});

    res.status(200).send("Successfully deleted!");
};

//========================Validator========================//

exports.memberIdValidator = (req, res, next) => {
    const memberId = req.params.memberId ? req.params.memberId : req.body.memberId;

    if (!memberId) {
        res.status(400).send('Please provide Member ID');
    } else {
        const isMemberExisting = memberDataStore.search({memberId});
        if (!isMemberExisting.length) {
            res.status(404).send('Member not existing');
        } else {
            next();
        }
    }
};

exports.searchMemberValidator = (req, res, next) => {
    const params = req.query;

    if (Object.keys(params).length === 0) {
        res.status(400).send('Please provide search criteria!');
    } else if(params.status && !['active', 'inactive'].includes(params.status.toLowerCase())){
        res.status(400).send('Active/Inactive only for status');
    } else {
        next();
    }
};

exports.memberValidator = (req, res, next) => {
    const requiredProps = ['name', 'status'];
    const bodyProps = Object.keys(req.body);

    const propsAreValid = requiredProps.every(propName => bodyProps.includes(propName) && req.body[propName]);

    if (!propsAreValid) {
        res.status(400).send('Missing properties');
    } else if (!['active', 'inactive'].includes(req.body['status'].toLowerCase()) ) {
        res.status(400).send('Active/Inactive only for status');
    } else {
        next();
    }
};