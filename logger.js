const moment = require('moment');
const EventEmitter = require('events');
const fs = require('fs');
const path = require('path');

class Logger extends EventEmitter {

    logAPICalls = (app) => {
        app.use((req, res, next) => {
            const outputsDir = path.join(__dirname, 'logs');
            if (!fs.existsSync(outputsDir)) {
                fs.mkdirSync(outputsDir);
            }
            
            const fileName = `${outputsDir}/AttendanceMonitoringLogs-${moment().format('YYYY-MM-DD')}.log`

            const datetime = moment().format('YYYY-MM-DD HH:mm');
            const params = req.params && Object.keys(req.params).length ? JSON.stringify(req.params) : '';
            const body = req.body && Object.keys(req.body).length? JSON.stringify(req.body) : '';

            const content = `${datetime} ${req.method} ${req.url} ${params} ${body}\n`;

            this.emit('log', {fileName, content});
            next();
        });
    }
}

module.exports = Logger;




