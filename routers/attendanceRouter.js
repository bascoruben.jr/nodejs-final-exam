const express = require('express');
const attendanceController = require('../controllers/attendanceController');

const router = express.Router();

router.post('/', attendanceController.attendanceValidator, attendanceController.insertAttendance);
router.put('/', attendanceController.attendanceIdValidator, attendanceController.attendanceValidator, attendanceController.updateAttendance);
router.delete('/', attendanceController.attendanceIdValidator, attendanceController.deleteAttendance);

module.exports = router