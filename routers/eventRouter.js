const express = require('express');
const eventController = require('../controllers/eventController');

const router = express.Router();

router.get('/', eventController.getAllEvents);
router.get('/search?', eventController.searchEventValidator, eventController.searchEvent);
router.get('/export/:eventId', eventController.eventIdValidator, eventController.exportEventMemberAttendance);
router.get('/:eventId', eventController.eventIdValidator, eventController.getEventById);
router.post('/', eventController.eventValidator, eventController.insertEvent);
router.put('/', eventController.eventIdValidator, eventController.eventValidator, eventController.updateEvent);
router.delete('/', eventController.eventIdValidator, eventController.deleteEvent);

module.exports = router