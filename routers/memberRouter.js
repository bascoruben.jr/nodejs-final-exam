const express = require('express');
const memberController = require('../controllers/memberController');

const router = express.Router();

router.get('/', memberController.getAllMembers);
router.get('/search?', memberController.searchMemberValidator, memberController.searchMember);
router.get('/:memberId', memberController.memberIdValidator, memberController.getMemberById);
router.post('/', memberController.memberValidator, memberController.insertMember);
router.put('/', memberController.memberIdValidator, memberController.memberValidator, memberController.updateMember);
router.delete('/', memberController.memberIdValidator, memberController.deleteMember);

module.exports = router